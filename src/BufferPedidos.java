import java.util.ArrayList;
import java.util.List;

public class BufferPedidos {

	private List<Pedido> pedidos;

	/**
	 * Cria um buffer de pedidos com o tamanho informado.
	 *
	 * @param tamanhoBuffer
	 */
	public BufferPedidos(int tamanhoBuffer) {
		this.pedidos = new ArrayList<Pedido>(tamanhoBuffer);
	}

	/**
	 * Cria um buffer de pedidos
	 */
	public BufferPedidos() {
		this.pedidos = new ArrayList<Pedido>();
	}

	/**
	 * Popula o buffer com todos os pedidos da colecao. <br>
	 *
	 * Obs.: O tamanho da colecao sera utilizado como o tamanho do buffer.
	 *
	 * @param pedidos
	 */
	public void popular(List<Pedido> pedidos) {
		if (pedidos != null) {
			this.pedidos.addAll(pedidos);
		}
	}

	/**
	 * Adiciona um novo pedido no buffer
	 *
	 * @param p
	 *            um pedido
	 */
	public void adicionaPedido(Pedido p) {
		this.pedidos.add(p);
	}

	/**
	 * Verifica se existem pedidos no buffer
	 *
	 * @return <code>true</code> se existir pedidos no buffer senão
	 *         <code>false</code>
	 */
	public boolean temPedidos() {
		return !this.pedidos.isEmpty();
	}

	/**
	 * Retorna o proximo pedido e remove do buffer
	 *
	 * @return o proximo pedido
	 */
	public Pedido proximoPedido() {
		return this.pedidos.remove(0);
	}

}
