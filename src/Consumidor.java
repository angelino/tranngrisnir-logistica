public class Consumidor extends Thread {

	private BufferPedidos buffer;
	private int delay = 10; // default
	private boolean flagLogProcessamento = false; // default

	/**
	 * Cria um novo consumidor para o buffer informado
	 *
	 * @param buffer
	 */
	public Consumidor(String nome, BufferPedidos buffer) {
		this.setName(nome);
		this.buffer = buffer;
	}

	@Override
	public void run() {
		super.run();

		while (buffer.temPedidos()) {
			processaPedido(buffer.proximoPedido());
		}

		// TODO: aguardar();
	}

	/**
	 * @param delay
	 *            tempo em milisegundos
	 */
	public void setDelay(int delay) {
		if (delay >= 0) { // positivo
			this.delay = delay;
		}
	}

	/**
	 *
	 * @param flagLogProcessamento
	 */
	public void setLogProcessamento(boolean flagLogProcessamento) {
		this.flagLogProcessamento = flagLogProcessamento;
	}

	private void processaPedido(Pedido p) {
		Cronometro contador = new Cronometro();
		contador.iniciar();

		delay();

		contador.parar();

		if (flagLogProcessamento) {
			log(p, contador);
		}
	}

	/**
	 * Simula o tempo do processamento
	 *
	 */
	private void delay() {
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			System.err.println(e.getStackTrace());
		}
	}

	/**
	 * Coloca a thread em modo de espera
	 */
	private void aguardar() {
		try {
			this.wait();
		} catch (InterruptedException e) {
			System.err.println(e.getStackTrace());
		}
	}

	private void log(Pedido pedido, Cronometro contador) {
		StringBuffer mensagem = new StringBuffer();
		mensagem.append("### ");
		mensagem.append("Processamento da Thread: " + this.getName());
		mensagem.append(" " + pedido);
		mensagem.append(" Inicio: " + contador.getDataHoraInicio());
		mensagem.append(" Fim: " + contador.getDataHoraFim());
		System.out.println(mensagem);
	}

}
