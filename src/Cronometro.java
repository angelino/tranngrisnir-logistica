import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Cronometro {

	private long tempoInicial;
	private long tempoFinal;

	private DateFormat formatoDataHora = new SimpleDateFormat(
			"dd/MM/yyyy HH:MM:SS");

	/**
	 * Inicia a contagem do tempo
	 */
	public void iniciar() {
		tempoInicial = System.currentTimeMillis();
	}

	/**
	 * Interrompe a contagem do tempo
	 */
	public void parar() {
		tempoFinal = System.currentTimeMillis();
	}

	/**
	 * Recupera o tempo total de processamento em milisegundos
	 *
	 * @return o tempo total
	 */
	public long getTempoTotalEmMilis() {
		return tempoFinal - tempoInicial;
	}

	/**
	 * Recupera a data hora do inicio do processamento no formato (dd/MM/yyyy
	 * HH:MM:SS)
	 *
	 * @return a data hora formatada do inicio do processamento
	 */
	public String getDataHoraInicio() {
		return formatoDataHora.format(new Date(tempoInicial));
	}

	/**
	 * Recupera a data hora do final do processamento no formato (dd/MM/yyyy
	 * HH:MM:SS)
	 *
	 * @return a data hora formatada do final do processamento
	 */
	public String getDataHoraFim() {
		return formatoDataHora.format(new Date(tempoFinal));
	}

}
