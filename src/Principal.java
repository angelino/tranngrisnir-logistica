import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Principal {

	// TODO: aumentar para 5000 para demonstracao
	private static final int TAMANHO_BUFFER_PEDIDOS = 100;

	// TODO: aumentar para 10000 para demonstracao
	private static final int DELAY_SIMULACAO_PROCESSAMENTO = 10;

	// TODO: Habilitar para demonstracao ou depuracao
	private static final boolean FLAG_LOG_PROCESSAMENTO = true;

	private static int[] THREADS_SIMULTANEAS = new int[] { 1, 10, 50, 500, 1000 };

	private static Random geradorValor = new Random(10);

	public static void main(String[] args) throws Exception {
		System.out.println("### Inicio do processamento...");

		List<Pedido> pedidos = criarPedidos();

		for (int qtdeThreads : THREADS_SIMULTANEAS) {
			System.out.println("### Processamento com " + qtdeThreads
					+ " Threads simultaneas...");

			BufferPedidos bufferPedidos = new BufferPedidos();
			bufferPedidos.popular(pedidos);

			Cronometro contador = new Cronometro();
			contador.iniciar();

			// Cria os consumidores olhando para o buffer de produtos
			List<Consumidor> consumidores = new ArrayList<Consumidor>();
			for (int i = 0; i < qtdeThreads; i++) {
				Consumidor c = new Consumidor("Consumidor " + i, bufferPedidos);
				c.setDelay(DELAY_SIMULACAO_PROCESSAMENTO);
				c.setLogProcessamento(FLAG_LOG_PROCESSAMENTO);
				consumidores.add(c);
			}

			// Inicia o processamento das Threads
			for (Consumidor c : consumidores) {
				c.start();
			}

			// Aguarda o processamento das threads consumidoras
			for (Consumidor c : consumidores) {
				// FIXME: Nao sei se eh o jeito certo de fazer
				c.join();
			}

			contador.parar();

			System.out.println("### Processamento com " + qtdeThreads
					+ " Threads simultaneas realizado em "
					+ contador.getTempoTotalEmMilis() + " milisegundos...");
			System.out.println("\n\n");
		}

		System.out.println("### Final do processamento");
	}

	private static List<Pedido> criarPedidos() {
		List<Pedido> pedidos = new ArrayList<Pedido>();

		for (int i = 0; i < TAMANHO_BUFFER_PEDIDOS; i++) {
			double valor = (geradorValor.nextInt(5)) * 100.0;
			pedidos.add(new Pedido("Pedido" + i, "Descricao " + i, valor));
		}

		return pedidos;
	}
}
